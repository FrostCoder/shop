const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const LeagueWins = sequelize.define('leagueoflegends_win', {

    id: {
        type: Sequelize.NUMERIC,
        primaryKey: true
    },

    division : {
        type: Sequelize.STRING
    },

    price : {
        type: Sequelize.NUMERIC
    },
    content : {
        type: Sequelize.STRING
    }
})

module.exports =  LeagueWins;