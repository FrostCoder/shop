const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const LeagueNormals = sequelize.define('leagueoflegends_normal', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    name : {
        type: Sequelize.STRING
    },

    price : {
        type: Sequelize.STRING
    }
})

module.exports = LeagueNormals;