const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const LeaguePlacement = sequelize.define('leagueoflegends_placement', {
    id: {
        type: Sequelize.NUMERIC,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.NUMERIC
    }
});

module.exports = LeaguePlacement;