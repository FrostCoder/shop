const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const LeagueLeveling = sequelize.define('leagueoflegends_levelings', {
    id : {
        type: Sequelize.STRING,
        primaryKey: true
    },
    name : {
        type: Sequelize.STRING
    },

    price: {
        type: Sequelize.NUMERIC
    }
})

module.exports = LeagueLeveling;