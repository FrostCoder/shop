const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const LeagueBoosting = sequelize.define('leagueoflegends_boosting', {
    id: {
        type: Sequelize.NUMERIC,
        primaryKey: true
    },
    division: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.NUMERIC
    },
    content: {
        type: Sequelize.STRING
    }
},{
    tableName: 'leagueoflegends_boostings'
});

module.exports = LeagueBoosting;