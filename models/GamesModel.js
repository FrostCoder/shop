const Sequelize = require('sequelize');
const sequelize = require('./DatabaseModel');

const GameList = sequelize.define('games', {
    id: {
        type: Sequelize.NUMERIC,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    link: {
        type: Sequelize.STRING
    }
});

module.exports = GameList;