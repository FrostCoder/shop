const Sequelize = require('sequelize');
const sequelize = require('../DatabaseModel');

const SkillRaiting = sequelize.define('overwatch_skillraitings', {
    id: {
        type: Sequelize.NUMERIC,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    price: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.STRING
    }
})

module.exports = SkillRaiting;