const Sequelize = require('sequelize');

const sequelize = new  Sequelize('ShopDb', 'postgres', 'password123', {
    host: 'localhost',
    dialect: 'postgres', 
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    define: {
        timestamps: false
    }
});

sequelize.authenticate()
    .then(()=> {console.log("Connection has been established successfully")})
    .catch(err=> {console.log("Unable to connect to database : " + err)});

module.exports = sequelize;