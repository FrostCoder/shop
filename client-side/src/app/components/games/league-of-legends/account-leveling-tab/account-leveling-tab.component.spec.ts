import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountLevelingTabComponent } from './account-leveling-tab.component';

describe('AccountLevelingTabComponent', () => {
  let component: AccountLevelingTabComponent;
  let fixture: ComponentFixture<AccountLevelingTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountLevelingTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountLevelingTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
