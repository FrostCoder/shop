import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'account-leveling-slider',
  templateUrl: './account-leveling-slider.component.html',
  styleUrls: ['./account-leveling-slider.component.scss']
})
export class AccountLevelingSliderComponent implements OnInit {

  @Output() changeAmount = new EventEmitter();
  @Input() min : number;
  @Input() max : number;
  @Input() step : number;
  @Input() value : number;


  constructor() {

  }

  ngOnInit() {

  }

  changeValue(event) {
    this.changeAmount.emit(event.value);
  }

}
