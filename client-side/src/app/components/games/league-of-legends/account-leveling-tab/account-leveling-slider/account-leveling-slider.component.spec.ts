import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountLevelingSliderComponent } from './account-leveling-slider.component';

describe('AccountLevelingSliderComponent', () => {
  let component: AccountLevelingSliderComponent;
  let fixture: ComponentFixture<AccountLevelingSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountLevelingSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountLevelingSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
