import { Component, OnInit, Input } from '@angular/core';
import { LeagueLeveling } from '../../../../models/league-of-legends/LeagueLeveling';

@Component({
  selector: 'account-leveling-tab',
  templateUrl: './account-leveling-tab.component.html',
  styleUrls: ['../league-of-legends.component.scss']
})
export class AccountLevelingTabComponent implements OnInit {

  @Input() leagueLeveling: LeagueLeveling[];

  public currPrice = 5;
  public currAmount = 1;

  //Slider
  public min = 1;
  public max = 15;
  public step = 1;
  public value = 1;

  constructor() { }

  ngOnInit() {

  }

  getAmount(event) {
    this.currAmount = event;
    this.calcPrice();
  }

  calcPrice() {

    return this.currPrice = this.leagueLeveling[0].price * this.currAmount;
  }
}
