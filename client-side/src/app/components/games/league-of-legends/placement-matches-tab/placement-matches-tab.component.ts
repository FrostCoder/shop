import { Component, OnInit, Input } from '@angular/core';
import { LeaguePlacements } from '../../../../models/league-of-legends/LeaguePlacements';
import { LeagueOfLegendsService } from '../../../../services/league-of-legends/league-of-legends.service';

@Component({
  selector: 'placement-matches-tab',
  templateUrl: './placement-matches-tab.component.html',
  styleUrls: ['../league-of-legends.component.scss'],
  providers: [LeagueOfLegendsService]
})
export class PlacementMatchesTabComponent implements OnInit {

  @Input() leaguePlacements: LeaguePlacements[];
  @Input() Leagues = [];

  public currLeague = 'silver';
  public currDiv = '_5';
  public currContent: string;
  public currPrice = 3;
  public currAmount = 1;

  //Slider
  public min = 1;
  public max = 10;
  public step = 1;
  public value = 1;


  constructor(private leagueOfLegendsService: LeagueOfLegendsService) {
  }

  ngOnInit() {

  }
  getAmount(event) {
    this.currAmount = event;
    this.calcPrice();
  }

  public getImage(content, league, division) {
    return this.leagueOfLegendsService.getLeagueImage(content, league, division);
  }

  public calcPrice() {
    return this.currPrice = (this.leaguePlacements[this.leaguePlacements.findIndex(data => data.name === this.currLeague)].price) * this.currAmount;
  }
}
