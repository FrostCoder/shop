import { Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { LeagueOfLegendsService } from '../../../../../services/league-of-legends/league-of-legends.service';

@Component({
  selector: 'placement-matches-slider',
  templateUrl: './placement-matches-slider.component.html',
  styleUrls: ['../../league-of-legends.component.scss'],
  providers: [LeagueOfLegendsService]
})
export class PlacementMatchesSliderComponent implements OnInit {

  @Output() changeAmount = new EventEmitter();
  @Input() min : number;
  @Input() max : number;
  @Input() step : number;
  @Input() value : number;


  constructor(private leagueOfLegendsService : LeagueOfLegendsService) {

   }

  ngOnInit() {
    
  }

  changeValue(event) {
    this.changeAmount.emit(event.value);
  }

}
