import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementMatchesSliderComponent } from './placement-matches-slider.component';

describe('PlacementMatchesSliderComponent', () => {
  let component: PlacementMatchesSliderComponent;
  let fixture: ComponentFixture<PlacementMatchesSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementMatchesSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementMatchesSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
