import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementMatchesTabComponent } from './placement-matches-tab.component';

describe('PlacementMatchesTabComponent', () => {
  let component: PlacementMatchesTabComponent;
  let fixture: ComponentFixture<PlacementMatchesTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementMatchesTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementMatchesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
