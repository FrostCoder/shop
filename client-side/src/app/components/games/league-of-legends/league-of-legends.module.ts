import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeagueOfLegendsService } from '../../../services/league-of-legends/league-of-legends.service';
import { AccountLevelingTabComponent } from './account-leveling-tab/account-leveling-tab.component';
import { AccountLevelingSliderComponent } from './account-leveling-tab/account-leveling-slider/account-leveling-slider.component';
import { DivisionBoostingTabComponent } from './division-boosting-tab/division-boosting-tab.component';
import { NormalMatchesTabComponent } from './normal-matches-tab/normal-matches-tab.component';
import { NormalMatchesSliderComponent } from './normal-matches-tab/normal-matches-slider/normal-matches-slider.component';
import { PlacementMatchesTabComponent } from './placement-matches-tab/placement-matches-tab.component';
import { PlacementMatchesSliderComponent} from './placement-matches-tab/placement-matches-slider/placement-matches-slider.component';
import { WinsBoostingTabComponent } from './wins-boosting-tab/wins-boosting-tab.component';
import { WinsBoostingSliderComponent } from './wins-boosting-tab/wins-boosting-slider/wins-boosting-slider.component';
import { LeagueOfLegendsComponent } from './league-of-legends.component';

import { MaterialModule } from '../../../material.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeagueOfLegendsRouting } from './league-of-legends.routing.module';



@NgModule({
    declarations: [
        AccountLevelingTabComponent,
        AccountLevelingSliderComponent,
        NormalMatchesTabComponent,
        NormalMatchesSliderComponent,
        DivisionBoostingTabComponent,
        PlacementMatchesTabComponent,
        PlacementMatchesSliderComponent,
        WinsBoostingTabComponent,
        WinsBoostingSliderComponent,
        LeagueOfLegendsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        MDBBootstrapModule.forRoot(),
        HttpModule,
        FormsModule,
        LeagueOfLegendsRouting
    ],
    exports: [
    ],
    providers: [LeagueOfLegendsService]
})

export class LeagueOfLegendsModule { }