import { Component, OnInit } from '@angular/core';
import { LeagueOfLegendsService } from '../../../services/league-of-legends/league-of-legends.service';
import { LeagueBoosting } from '../../../models/league-of-legends/LeagueBoosting';
import { LeaguePlacements } from '../../../models/league-of-legends/LeaguePlacements';
import { LeagueWins } from '../../../models/league-of-legends/LeagueWins';
import { LeagueNormals } from '../../../models/league-of-legends/LeagueNormals';
import { LeagueLeveling } from '../../../models/league-of-legends/LeagueLeveling';

@Component({
  selector: 'app-league-of-legends',
  templateUrl: './league-of-legends.component.html',
  styleUrls: ['./league-of-legends.component.scss'],
  providers: [LeagueOfLegendsService],
  preserveWhitespaces: false
})

export class LeagueOfLegendsComponent implements OnInit {

  public Leagues = [
    { value: 'diamond', name: 'Diamond' },
    { value: 'platinum', name: 'Platinum' },
    { value: 'gold', name: 'Gold' },
    { value: 'silver', name: 'Silver' },
    { value: 'bronze', name: 'Bronze' }
  ]

  public Divisions = [
    { value: '_1', name: 'Division I' },
    { value: '_2', name: 'Division II' },
    { value: '_3', name: 'Division III' },
    { value: '_4', name: 'Division IV' },
    { value: '_5', name: 'Division V' }
  ]

  //League Boosting list
  public leagueBoosting: LeagueBoosting[];
  //League Placements list
  public leaguePlacements: LeaguePlacements[];
  //League Wins list
  public leagueWins: LeagueWins[];
  //League Normals list
  public leagueNormals: LeagueNormals[];
  //league Leveling list
  public leagueLeveling: LeagueLeveling[];

  constructor(private leagueOfLegendsService: LeagueOfLegendsService) {
  }

  ngOnInit() {
    //On load GET
    this.leagueOfLegendsService.getLeagueOfLegendsPriceList().subscribe(data => {
      this.leagueBoosting = data.LeagueBoosting;
      this.leagueLeveling = data.LeagueLeveling;
      this.leagueNormals = data.LeagueNormals;
      this.leaguePlacements = data.LeaguePlacements;
      this.leagueWins = data.LeagueWins;
    });

  }
}
