import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionBoostingTabComponent } from './division-boosting-tab.component';

describe('DivisionBoostingTabComponent', () => {
  let component: DivisionBoostingTabComponent;
  let fixture: ComponentFixture<DivisionBoostingTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisionBoostingTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionBoostingTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
