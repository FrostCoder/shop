import { Component, OnInit, Input } from '@angular/core';
import { LeagueBoosting } from '../../../../models/league-of-legends/LeagueBoosting';
import { LeagueOfLegendsService } from '../../../../services/league-of-legends/league-of-legends.service';

@Component({
  selector: 'division-boosting-tab',
  templateUrl: './division-boosting-tab.component.html',
  styleUrls: ['../league-of-legends.component.scss'],
  providers: [LeagueOfLegendsService]
})

export class DivisionBoostingTabComponent implements OnInit {

  @Input() Leagues = [];
  @Input() Divisions = [];
  @Input() leagueBoosting: LeagueBoosting[];

  public currLeague = 'platinum';
  public currDiv = '_2';
  public currContent : string;
  public desLeague = 'diamond';
  public desDiv = '_5';
  public desContent : string;
  public currentIndex = 18;
  public desiredIndex = 20;
  public currvPrice = 104;


  constructor(private leagueOfLegendsService : LeagueOfLegendsService) { }

  ngOnInit() {

  }

  public getImage(content, league, division ){
    return this.leagueOfLegendsService.getLeagueImage(content, league, division);
  } 

  public calcLeagueBoosting() {
    let price = [];
    let indexFrom = this.leagueOfLegendsService.getIndexOf(this.currentIndex, this.leagueBoosting, this.currLeague, this.currDiv);
    let indexTo = this.leagueOfLegendsService.getIndexOf(this.desiredIndex, this.leagueBoosting, this.desLeague, this.desDiv);
    let result: number;
    for (let i = indexFrom; i < indexTo; i++) {
      price.push(Number(this.leagueBoosting[i].price));
    }
    if (price != null) {
      result = price.reduce((a, b) => {
        return a + b;
      });
    }
    return this.currvPrice = result;
  }
}