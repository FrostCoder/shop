import { Component, OnInit, Input } from '@angular/core';
import { LeagueNormals } from '../../../../models/league-of-legends/LeagueNormals';

@Component({
  selector: 'normal-matches-tab',
  templateUrl: './normal-matches-tab.component.html',
  styleUrls: ['../league-of-legends.component.scss']
})
export class NormalMatchesTabComponent implements OnInit {

  @Input() leagueNormals: LeagueNormals[];

  public currPrice = 2;
  public currAmount = 1;

  //Slider
  public min = 1;
  public max = 40;
  public step = 1;
  public value = 1;

  constructor() { }

  ngOnInit() {
  }

  getAmount(event) {
    this.currAmount = event;
    this.calcPrice();
  }

  calcPrice() {
    return this.currPrice = this.leagueNormals[0].price * this.currAmount;
  }

}
