import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalMatchesSliderComponent } from './normal-matches-slider.component';

describe('NormalMatchesSliderComponent', () => {
  let component: NormalMatchesSliderComponent;
  let fixture: ComponentFixture<NormalMatchesSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalMatchesSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalMatchesSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
