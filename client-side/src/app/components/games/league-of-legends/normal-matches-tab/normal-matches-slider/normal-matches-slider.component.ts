import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {  } from 'events';

@Component({
  selector: 'normal-matches-slider',
  templateUrl: './normal-matches-slider.component.html',
  styleUrls: ['./normal-matches-slider.component.scss'],
})
export class NormalMatchesSliderComponent implements OnInit {

  @Output() changeAmount = new EventEmitter();
  @Input() min : number;
  @Input() max : number;
  @Input() step : number;
  @Input() value : number;

  constructor() {

   }

  ngOnInit() {
    
  }

  changeValue(event) {
    this.changeAmount.emit(event.value);
  }


}
