import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalMatchesTabComponent } from './normal-matches-tab.component';

describe('NormalMatchesTabComponent', () => {
  let component: NormalMatchesTabComponent;
  let fixture: ComponentFixture<NormalMatchesTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalMatchesTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalMatchesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
