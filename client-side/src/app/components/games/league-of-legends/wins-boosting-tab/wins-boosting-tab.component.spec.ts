import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinsBoostingTabComponent } from './wins-boosting-tab.component';

describe('WinsBoostingTabComponent', () => {
  let component: WinsBoostingTabComponent;
  let fixture: ComponentFixture<WinsBoostingTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinsBoostingTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinsBoostingTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
