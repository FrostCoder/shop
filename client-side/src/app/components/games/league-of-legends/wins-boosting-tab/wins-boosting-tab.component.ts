import { Component, OnInit, Input } from '@angular/core';
import { LeagueWins } from '../../../../models/league-of-legends/LeagueWins';
import { LeagueOfLegendsService } from '../../../../services/league-of-legends/league-of-legends.service';

@Component({
  selector: 'wins-boosting-tab',
  templateUrl: './wins-boosting-tab.component.html',
  styleUrls: ['../league-of-legends.component.scss'],
  providers: [LeagueOfLegendsService]
})
export class WinsBoostingTabComponent implements OnInit {

  @Input() Leagues = [];
  @Input() Divisions = [];
  @Input() leagueWins: LeagueWins[];

  constructor(private leagueOfLegendsService: LeagueOfLegendsService) { }

  //Slider
  public min = 1;
  public max = 20;
  public step = 1;
  public value = 1;

  ngOnInit() {

  }

  public currLeague = 'silver';
  public currDiv = '_2';
  public currContent = '';
  public currPrice = 5;
  public currAmount =  1;

  getAmount(event: any) {
   this.currAmount = event;
   this.calcPrice();
  }
  getImage(content: any, league: any, division: any) {
    return this.leagueOfLegendsService.getLeagueImage(content, league, division);
  }
  calcPrice() {
  
    return this.currPrice = (this.leagueWins[this.leagueWins.findIndex(data => data.content === '/images/' + this.currLeague + this.currDiv + '.png')].price) * this.currAmount;
  }

}
