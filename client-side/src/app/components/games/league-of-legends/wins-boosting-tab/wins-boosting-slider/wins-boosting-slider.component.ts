import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'wins-boosting-slider',
  templateUrl: './wins-boosting-slider.component.html',
  styleUrls: ['./wins-boosting-slider.component.scss']
})
export class WinsBoostingSliderComponent implements OnInit {

  @Output() changeAmount = new EventEmitter();
  @Input() min : number;
  @Input() max : number;
  @Input() step : number;
  @Input() value : number;

  constructor() { }

  ngOnInit() {
  }
  
  changeValue(event) {
    this.changeAmount.emit(event.value);
  }


}
