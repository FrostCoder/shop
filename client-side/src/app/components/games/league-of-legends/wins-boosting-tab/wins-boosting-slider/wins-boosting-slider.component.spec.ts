import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinsBoostingSliderComponent } from './wins-boosting-slider.component';

describe('WinsBoostingSliderComponent', () => {
  let component: WinsBoostingSliderComponent;
  let fixture: ComponentFixture<WinsBoostingSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinsBoostingSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinsBoostingSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
