import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbionOnlineComponent } from './albion-online.component';

describe('AlbionOnlineComponent', () => {
  let component: AlbionOnlineComponent;
  let fixture: ComponentFixture<AlbionOnlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbionOnlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbionOnlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
