import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorldOfWarcraftComponent } from './world-of-warcraft.component';

describe('WorldOfWarcraftComponent', () => {
  let component: WorldOfWarcraftComponent;
  let fixture: ComponentFixture<WorldOfWarcraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorldOfWarcraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorldOfWarcraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
