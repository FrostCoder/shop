import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PathOfExileComponent } from './path-of-exile.component';

describe('PathOfExileComponent', () => {
  let component: PathOfExileComponent;
  let fixture: ComponentFixture<PathOfExileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PathOfExileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PathOfExileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
