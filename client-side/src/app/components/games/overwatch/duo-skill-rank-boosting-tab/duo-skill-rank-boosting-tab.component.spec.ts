import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuoSkillRankBoostingTabComponent } from './duo-skill-rank-boosting-tab.component';

describe('DuoSkillRankBoostingTabComponent', () => {
  let component: DuoSkillRankBoostingTabComponent;
  let fixture: ComponentFixture<DuoSkillRankBoostingTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuoSkillRankBoostingTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuoSkillRankBoostingTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
