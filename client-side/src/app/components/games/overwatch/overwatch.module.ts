import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonRangeSliderModule, IonRangeSliderComponent } from "ng2-ion-range-slider";

import { AccountLevelingTabComponent } from './account-leveling-tab/account-leveling-tab.component';
import { AntiDecayTabComponent } from './anti-decay-tab/anti-decay-tab.component';
import { DuoSkillRankBoostingTabComponent } from './duo-skill-rank-boosting-tab/duo-skill-rank-boosting-tab.component';
import { PlacementGamesTabComponent } from './placement-games-tab/placement-games-tab.component';
import { SkillRankBoostingTabComponent } from './skill-rank-boosting-tab/skill-rank-boosting-tab.component';
import { SkillRankBoostingSliderComponent } from './skill-rank-boosting-tab/skill-rank-boosting-slider/skill-rank-boosting-slider.component';
import { OverwatchComponent } from './overwatch.component';

import { MaterialModule } from '../../../material.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverwatchRouting } from './overwatch.routing.module';


@NgModule({

    declarations: [
        IonRangeSliderComponent,
        AccountLevelingTabComponent,
        AntiDecayTabComponent,
        DuoSkillRankBoostingTabComponent,
        PlacementGamesTabComponent,
        SkillRankBoostingTabComponent,
        SkillRankBoostingSliderComponent,
        OverwatchComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        MDBBootstrapModule.forRoot(),
        HttpModule,
        FormsModule,
        OverwatchRouting

    ],
    exports: [],
    providers: []
})

export class OverwatchModule { }