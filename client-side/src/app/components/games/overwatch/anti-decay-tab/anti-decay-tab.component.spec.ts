import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AntiDecayTabComponent } from './anti-decay-tab.component';

describe('AntiDecayTabComponent', () => {
  let component: AntiDecayTabComponent;
  let fixture: ComponentFixture<AntiDecayTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AntiDecayTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AntiDecayTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
