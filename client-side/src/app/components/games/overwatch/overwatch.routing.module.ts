import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { OverwatchComponent } from './overwatch.component';

const routes: Routes = [
    { path: '', component: OverwatchComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class OverwatchRouting { }