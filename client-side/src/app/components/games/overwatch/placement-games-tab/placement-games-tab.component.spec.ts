import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlacementGamesTabComponent } from './placement-games-tab.component';

describe('PlacementGamesTabComponent', () => {
  let component: PlacementGamesTabComponent;
  let fixture: ComponentFixture<PlacementGamesTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacementGamesTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlacementGamesTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
