import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillRankBoostingTabComponent } from './skill-rank-boosting-tab.component';

describe('SkillRankBoostingTabComponent', () => {
  let component: SkillRankBoostingTabComponent;
  let fixture: ComponentFixture<SkillRankBoostingTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillRankBoostingTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillRankBoostingTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
