import { Component, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'skill-rank-boosting-slider',
  templateUrl: './skill-rank-boosting-slider.component.html',
  styleUrls: ['./skill-rank-boosting-slider.component.scss']
})
export class SkillRankBoostingSliderComponent {

  @Output() currentAmount = new EventEmitter();
  @Output() desiredAmount = new EventEmitter();


  @Input() min: number;
  @Input() max: number;
  @Input() from: number;
  @Input() to: number;
  @Input() type : string;
  @Input() from_min : number;
  @Input() from_max : number;
  @Input() to_min : number;
  @Input() to_max : number;
  @Input() hide_min_max : number;
  @Input() hide_from_to : number;


  changeValue(event) {
    this.currentAmount.emit(event.from);
    this.desiredAmount.emit(event.to);
  }

  checkValues(event) {
    if (event.from == event.to || event.to - event.from < 50) {
      console.log('change');
      return this.from_max = event.to - 50;
    }
    else {
      return this.from_max = 4400;
    }
  }


}
