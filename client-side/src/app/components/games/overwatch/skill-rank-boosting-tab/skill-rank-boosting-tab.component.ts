import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'skill-rank-boosting-tab',
  templateUrl: './skill-rank-boosting-tab.component.html',
  styleUrls: ['../overwatch.component.scss']
})
export class SkillRankBoostingTabComponent implements OnInit {

  //Slider settings
  public from = 0;
  public to = 500;
  public min = 0;
  public max = 4450;
  public currentAmount = this.from;
  public desiredAmount = this.to;
  public type = "double";
  public from_min = 0;
  public from_max = 4400;
  public to_min = 50;
  public to_max = 4450;
  public hide_min_max = true;
  public hide_from_to = true;


  constructor() { }

  ngOnInit() {
  }

  getCurrentAmount(event) {
    this.currentAmount = event;
  }

  getDesiredAmount(event) {
    this.desiredAmount = event;
  }
}
