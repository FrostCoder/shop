import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WorldOfWarcraftComponent } from './world-of-warcraft/world-of-warcraft.component';
import { PathOfExileComponent } from './path-of-exile/path-of-exile.component';
import { OverwatchComponent } from './overwatch/overwatch.component';
import { HearthstoneComponent } from './hearthstone/hearthstone.component';
import { AlbionOnlineComponent } from './albion-online/albion-online.component';


const routes: Routes = [

  { path: 'league-of-legends', loadChildren: './league-of-legends/league-of-legends.module#LeagueOfLegendsModule' },
  { path: 'world-of-warcraft', component: WorldOfWarcraftComponent },
  { path: 'path-of-exile', component: PathOfExileComponent },
  { path: 'overwatch', loadChildren: './overwatch/overwatch.module#OverwatchModule' },
  { path: 'hearthstone', component: HearthstoneComponent },
  { path: 'albion-online', component: AlbionOnlineComponent },
]
@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  entryComponents: []
})

export class GamesRoutingModule { }
