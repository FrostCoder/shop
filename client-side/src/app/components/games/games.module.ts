import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { LeagueOfLegendsComponent } from './league-of-legends/league-of-legends.component';
import { WorldOfWarcraftComponent } from './world-of-warcraft/world-of-warcraft.component';
import { PathOfExileComponent } from './path-of-exile/path-of-exile.component';
import { HearthstoneComponent } from './hearthstone/hearthstone.component';
import { AlbionOnlineComponent } from './albion-online/albion-online.component';

import { GameService } from '../../services/game/game.service';


import { GamesRoutingModule } from './games.routing.module';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from '../../material.module';

@NgModule({
    declarations: [
        WorldOfWarcraftComponent,
        PathOfExileComponent,
        HearthstoneComponent,
        AlbionOnlineComponent
    ],
    imports: [
         CommonModule,    
         GamesRoutingModule,
         MaterialModule,
         MDBBootstrapModule.forRoot(),
         HttpModule,
         FormsModule
        ],
    exports: [],
    providers: [GameService],
    entryComponents: [
        
    ]
})
export class GamesModule {}