import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  public interval: number = 4000;
  public activeSliderIndex: number = 0;
  public noWrapSlides: boolean = false;

  activeSLidechange(){
    console.log(this.activeSliderIndex);
  }

  public slides: Array<Object> = [
    {"image" : "/content/carousel/hearthstone.jpg"},
    {"image" : "/content/carousel/leagueoflegends.jpg"}
  ]
  constructor() { }

  ngOnInit() {
  }

}
