import { Component, OnInit } from '@angular/core';
import { GameService } from '../../services/game/game.service';
import { Game } from '../../models/game';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  providers: [GameService]
})
export class FooterComponent implements OnInit {
  games: Game[]
  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.getGameList().subscribe(games => this.games = games);
  }
}
