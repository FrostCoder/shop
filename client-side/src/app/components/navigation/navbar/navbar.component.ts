import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GameService } from '../../../services/game/game.service';
import { Game } from  '../../../models/game';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [GameService]
})
export class NavbarComponent implements OnInit {

  games: Game []
  constructor(private gameService: GameService) { }

  ngOnInit() {
    this.gameService.getGameList().subscribe(games => this.games = games);
  }

}
