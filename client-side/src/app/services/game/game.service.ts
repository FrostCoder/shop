import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Game } from '../../models/game';
import 'rxjs/add/operator/map';

@Injectable()
export class GameService {

  constructor(private http: Http) { }

  getGameList()  {
    return this.http.get('/api/games').map(res => res.json());
  }
}
