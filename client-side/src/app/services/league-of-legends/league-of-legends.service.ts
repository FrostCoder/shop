import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LeagueOfLegendsService {


  constructor(private http: Http) {

  }


  //Getting All prices
  getLeagueOfLegendsPriceList() {
    return this.http.get('/api/league-of-legends').map(res => res.json())
  }
 
  //Getting Boosting list
  getLeagueBoostingList() {
    return this.http.get('/api/league-of-legends/league-boosting').map(res => res.json());
  }

  //Calculating League Boost price

  calcLeagueBoosting(calc) {
    return this.http.post('/api/league-of-legends/league-boosting/calc/', calc).map(res => res.json());
  }

  //Getting League Placements list
  getLeaguePlacementsList() {
    return this.http.get('/api/league-of-legends/league-placements').map(res => res.json());
  }

  //Gettng League Wins list
  getLeagueWinsList() {
    return this.http.get('/api/league-of-legends/league-wins').map(res => res.json());
  }

  //Getting League Normals list
  getLeagueNormalsList() {
    return this.http.get('/api/league-of-legends/league-normals').map(res => res.json());
  }

  //Getting League Leveling list
  getLeagueLevelingList() {
    return this.http.get('/api/league-of-legends/league-leveling').map(res => res.json());
  }

  //Getting League Image
  getLeagueImage(content, league, divison) {
    return content = '/content/images/' + league + divison + '.png';
  }

  //Getting index of current or desired league
  getIndexOf(index, array, league, div) {
    return index = array.findIndex(value => value.content === '/images/' + league + div + '.png')
  }
}
