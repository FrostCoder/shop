import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { AppComponent } from './app.component';
import {HttpModule} from '@angular/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

import { NavbarComponent } from './components/navigation/navbar/navbar.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { GameMenuComponent } from './components/navigation/game-menu/game-menu.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { JobsComponent } from './components/navigation/jobs/jobs.component';
import { ContactUsComponent } from './components/navigation/contact-us/contact-us.component';
import { FaqComponent } from './components/navigation/faq/faq.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CarouselComponent,
    GameMenuComponent,
    ContentComponent,
    FooterComponent,
    JobsComponent,
    ContactUsComponent,
    FaqComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    AppRoutingModule,
    FormsModule
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
