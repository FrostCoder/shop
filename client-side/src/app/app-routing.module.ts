import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LeagueOfLegendsComponent } from './components/games/league-of-legends/league-of-legends.component';
import { WorldOfWarcraftComponent } from './components/games/world-of-warcraft/world-of-warcraft.component';
import { PathOfExileComponent } from './components/games/path-of-exile/path-of-exile.component';
import { OverwatchComponent } from './components/games/overwatch/overwatch.component';
import { HearthstoneComponent } from './components/games/hearthstone/hearthstone.component';
import { AlbionOnlineComponent } from './components/games/albion-online/albion-online.component';
import { ContentComponent } from './components/content/content.component';
import { JobsComponent } from './components/navigation/jobs/jobs.component';
import { ContactUsComponent } from './components/navigation/contact-us/contact-us.component';
import { FaqComponent } from './components/navigation/faq/faq.component';

const routes: Routes = [

  { path: 'games', loadChildren: './components/games/games.module#GamesModule' },
  { path: '', component: ContentComponent },
  { path: 'jobs', component: JobsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'faq', component: FaqComponent }
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }
