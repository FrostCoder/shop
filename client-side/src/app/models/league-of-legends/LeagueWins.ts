export class LeagueWins {
    id: number;
    division: string;
    price: number;
    content: string;
}