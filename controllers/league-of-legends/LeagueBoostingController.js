const promise = require('bluebird');
const LeagueBoosting = require('../../models/league-of-legends/LeagueBoostingModel');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
module.exports = {
    getLeagueBoostingList: function (req, res, next) {
        LeagueBoosting.findAll({
            order: ['id']
        })
            .then(leagues => {
                res.status(200).json(leagues);
            })
            .catch(err => {
                console.log(err);
                return next(err);
            })
    },

    calcLeagueBoosting: function (req, res, next) {
        let newCalc = ({
            from: req.body.from,
            to: req.body.to
        });
        console.log(newCalc);
        LeagueBoosting.sum('price', {
            where: {
                id: {
                    [Op.gt]: newCalc.from,
                    [Op.lt]: newCalc.to + 1
                }
            }
        })
            .then(leagues => {
                return res.status(200).json(leagues);
            })
            .catch(err => {
                return res.send();
            })
    }
}