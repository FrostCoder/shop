const sequelize = require('sequelize');
const promise = require('bluebird');
const LeagueBoosting = require('../../models/league-of-legends/LeagueBoostingModel');
const LeaguePlacements = require('../../models/league-of-legends/LeaguePlacementsModel');
const LeagueLeveling = require('../../models/league-of-legends/LeagueLevelingModel');
const LeagueNormals = require('../../models/league-of-legends/LeagueNormalsModel');
const LeagueWins = require('../../models/league-of-legends/LeagueWinsModel');

module.exports = {


    getLeagueOfLegendsPriceList : function (req, res, next) {
        Promise.all([LeagueNormals.findAll({order: ['id']}), LeagueLeveling.findAll({order: ['id']}), LeaguePlacements.findAll({order: ['id']}), LeagueBoosting.findAll({order: ['id']}), LeagueWins.findAll({order: ['id']})])
            .then(data => {
                res.status(200).json({
                    LeagueNormals: data[0],
                    LeagueLeveling: data[1],
                    LeaguePlacements: data[2],
                    LeagueBoosting: data[3],
                    LeagueWins: data[4]
                })

            })
            .catch(err => {
                console.log(err);
                return next(err);
            })
    }
}