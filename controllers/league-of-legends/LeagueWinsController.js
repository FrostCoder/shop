const promise = require('bluebird');
const LeagueWins = require('../../models/league-of-legends/LeagueWinsModel');
const Sequelize = require('sequelize');

module.exports = {
    getLeagueWinsList : function (req, res, next ) {
        LeagueWins.findAll({
            order: ['id']
        })
        .then(data =>  {
            res.status(200).json(data);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        })
    }
}