const promise = require('bluebird');
const LeaguePlacement = require('../../models/league-of-legends/LeaguePlacementsModel');
const Sequelize = require('sequelize');

module.exports = {
    getLeaguePlaecementsList: function (req, res, next ) {
        LeaguePlacement.findAll({
            order: ['id']
        })
        .then(data=> {
            res.status(200).json(data);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        })
    }
}