const promise = require('bluebird');
const LeagueLeveling = require('../../models/league-of-legends/LeagueLevelingModel');
const Sequelize = require('sequelize');

module.exports = {
    getLeagueLevelingList : function (req, res ,next) {
        LeagueLeveling.findAll({
            order: ['id']
        })
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        })
    }
}