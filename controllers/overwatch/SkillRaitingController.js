const promise = require('bluebird');
const SkillRaiting = require('../../models/overwatch/SkillRaitingModel');


module.exports = {
    getSkillRaitingList : function(req, res, next) {
        SkillRaiting.findAll({
            order: ['id']
        })
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        })
    }
}