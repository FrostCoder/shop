const promise = require('bluebird');
const GameList = require('../models/GamesModel');

module.exports = {
    getGameList : function (req, res, next) {
        GameList.findAll({
            order: ['id']
        })
            .then(games => {
                res.status(200).json(games);
            })
            .catch(err => {
                console.log(err);
                return next(err);
            })
    }
}