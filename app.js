const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const logger = require('morgan');
const router = require('./routes/routes');

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.json());
app.use(logger('dev'));
app.use('/api', router);

app.use(express.static(path.join(__dirname, 'public')));
app.listen(port, ()=>{
    console.log("Server working on port : " + port);
});
