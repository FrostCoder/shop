
const express = require('express');

//Controllers
const GameListController = require('../controllers/GamesController');

//League of legends Conrtollers
const LeagueBoostingController = require('../controllers/league-of-legends/LeagueBoostingController');
const LeaguePlacementsController = require('../controllers/league-of-legends/LeaguePlacementsController');
const LeagueWinsController = require('../controllers/league-of-legends/LeagueWinsController');
const LeagueNormalsController = require('../controllers/league-of-legends/LeagueNormalsController');
const LeagueLevelingController = require('../controllers/league-of-legends/LeagueLevelingConrtoller');
//All league of legends data
const LeagueOfLegendsController = require('../controllers/league-of-legends/LeagueOfLegendsConrtoller');

//Overwatch Controllers
const SkillRaitingController = require('../controllers/overwatch/SkillRaitingController');
//TODO: Overwatch, Worlf of Warcraft, Albion Online, Path of Exile, HearthStone
//
//
//
//World of Wacraft Controllers
//
//
//

const router = express.Router();

//Home page
router.get('/', (req, res)=> {
    res.send('index.html');
});

//Gettin gamelist data from database

router.get('/games',  GameListController.getGameList);

//League Of Legends Routes
//Getting List of all league of legends models
router.get('/league-of-legends', LeagueOfLegendsController.getLeagueOfLegendsPriceList);
//Getting league boosting data from DB
router.get('/league-of-legends/league-boosting', LeagueBoostingController.getLeagueBoostingList);
//Calculating League Boosting request
router.post('/league-of-legends/league-boosting/calc', LeagueBoostingController.calcLeagueBoosting);
//Getting League Placements List from DB
router.get('/league-of-legends/league-placements', LeaguePlacementsController.getLeaguePlaecementsList);
//Getting League Wins list from DB
router.get('/league-of-legends/league-wins', LeagueWinsController.getLeagueWinsList);
//Getting League normal drafts from DB
router.get('/league-of-legends/league-normals', LeagueNormalsController.getLeagueNormalsList, LeagueLevelingController.getLeagueLevelingList);
//Getting League levelings from DB
router.get('/league-of-legends/league-leveling', LeagueLevelingController.getLeagueLevelingList);

//Overwatch Routes
//Getting Skill Raiting list from DB
router.get('/overwatch/skill-raiting', SkillRaitingController.getSkillRaitingList);
module.exports  = router;